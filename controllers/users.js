'use strict';
const { getTransport, showError } = require('../utilitarios');
const { getConnectionDB } = require('../pgConnection');
const EmailValidator = require("email-validator");
const nodemailer = require('nodemailer');
const jwt = require('jsonwebtoken');
const Twig = require('twig');
const md5 = require('md5');

module.exports = {

    login: async (req, res, next) => {
        const con = await getConnectionDB();
        const { email, senha } = req.body;
        try {
            if ( !EmailValidator.validate(email) ) throw ('É necessário ter o e-mail válido.');
            if ( !(/^(\d{6,})$/gi.test(senha)) ) throw ('É necessário ter a senha.');
            var { rows } = await con.query(`SELECT id FROM usuario WHERE email = $1 AND senha = $2;`, [email, md5(`${senha}${process.env.HASH_MD5}`)]);
            if (!rows.length) throw ('Usuário ou senha incorretos.');
            const user = rows[0];
            const token = await jwt.sign({ user }, process.env.SECRET_KEY);
            con.end();
            res.json(token);
            next();
        } catch (error) {
            con.end(); showError(res, error);
        }
    },

    getAll: async (req, res, next) => {
        const con = await getConnectionDB();
        try {
            var { rows } = await con.query(`SELECT * FROM usuario;`, []);
            con.end();
            res.json(rows);
            next();
        } catch (error) {
            con.end(); showError(res, error);
        }
    },

    newUser: async (req, res, next) => {
        const con = await getConnectionDB();
        const transp = await getTransport(nodemailer);
        const { nome, email, senha, resenha } = req.body;
        try {
            if ( !(/([A-Za-z0-9.]{1,})$/gi.test(nome)) ) throw ('É necessário ter o nome.');
            if ( !EmailValidator.validate(email) ) throw ('É necessário ter o e-mail válido.');
            if ( !(/^(\d{6,})$/gi.test(senha)) ) throw ('É necessário ter a senha.');
            if ( !(/^(\d{6,})$/gi.test(resenha)) ) throw ('É necessário ter a confirmação da senha.');
            if (senha !== resenha) throw ('As senhas precisam ser iguais.');

            var rows = await con.query(`INSERT INTO usuario (nome, email, senha) VALUES ($1, $2, $3);`, [nome, email, md5(`${senha}${process.env.HASH_MD5}`)]);
            if ( !rows.rowCount ) throw ('Falha ao cadastrar usuário.');

            const { response } = await transp.sendMail({
                from: '"NOME" <no-replay@dominio.com.br>',
                to: email,
                subject: 'Seja bem vindo!',
                html: await new Promise((resolve) => { Twig.renderFile('./emails/usuario-boas-vindas.twig', { nome }, (err, html) => resolve(html))})
            });
            if (!response.includes('Ok:')) throw ('Erro ao enviar email.');

            con.end();
            res.status(201);
            res.json({ message: 'Cadastrado!' });
            next();
        } catch (error) {
            con.end(); showError(res, error);
        }
    },

    getUser: async (req, res, next) => {
        const con = await getConnectionDB();
        const { id } = req.params;
        try {
            if ( !(/^([0-9]{1,})$/.test(id)) ) throw ('É necessário um ID de usuário válido.');
            var { rows } = await con.query(`SELECT * FROM usuario WHERE id = $1;`, [id]);
            if (!rows.length) throw ('Usuário não encontrado.');
            const user = rows[0];
            con.end();
            res.json(user);
            next();
        } catch (error) {
            con.end(); showError(res, error);
        }
    },

    updateUser: async (req, res, next) => {
        const con = await getConnectionDB();
        const { id } = req.params;
        const { nome, email } = req.body;
        try {
            if ( !(/^([0-9]{1,})$/.test(id)) ) throw ('É necessário um ID de usuário válido.');
            if ( !(/([A-Za-z0-9.]{1,})$/gi.test(nome)) ) throw ('É necessário ter o nome.');
            if ( !EmailValidator.validate(email) ) throw ('É necessário ter o e-mail válido.');
            var rows = await con.query(`UPDATE usuario SET nome = $1, email = $2 WHERE id = $3;`, [nome, email, id]);
            if ( !rows.rowCount ) throw ('Não foi possivel atualizar.');
            con.end();
            res.json({ message: 'Usuário atualizado.' });
            next();
        } catch (error) {
            con.end(); showError(res, error);
        }
    }

};