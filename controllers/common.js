'use strict';
const { showError } = require('../utilitarios');

module.exports = {

    something: async (req, res, next) => {
        try {
            const arr = [
                {
                    nome: 'Teste',
                    email: 'teste@email.com.br'
                }
            ]
            res.json(arr);
            next();
        } catch (error) {
            showError(res, error);
        }
    }

};