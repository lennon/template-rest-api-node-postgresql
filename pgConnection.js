'use strict';
require('dotenv/config');

module.exports = {
    getConnectionDB: async () => {
        const { Client } = require('pg');
        try {
            const connection = await new Client({
                user: process.env.USER_DB,
                host: process.env.HOST_DB,
                database: process.env.DATABASE_DB,
                password: process.env.PASSWORD_DB,
                port: process.env.PORT_DB,
            });
            await connection.connect();
            return connection;
        } catch(e) {
            console.error('Não foi possível conectar com o Banco de Dados.', e);
            return null;
        }
    }
}