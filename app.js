'use strict';
require('dotenv/config');
const fs = require('fs');
const path = require('path');
const helmet = require('helmet');
const morgan = require('morgan');
const trim = require('trim-request-body');
const corsMiddleware = require('restify-cors-middleware');
const cors = corsMiddleware({ origins: ['*'], allowHeaders: ['*'], exposeHeaders: ['*'] });

// IMPORT ROUTES
const restify = require('restify');
const commonRouter = require('./routes/common');
const userRouter = require('./routes/users');

// CRIAÇÃO DO EXPRESS
const pasta_ssl_servidor = '/etc/letsencrypt/live/dominio.com.br'; // Pasta onde fica os arquivos criados pelo letsencrypt (não se esqueça de dar permissão total para a pasta).
const server = (process.env.NODE_ENV === 'development') ? restify.createServer() : restify.createServer({ key: fs.readFileSync(`${pasta_ssl_servidor}/privkey.pem`), cert: fs.readFileSync(`${pasta_ssl_servidor}/fullchain.pem`), ca: fs.readFileSync(`${pasta_ssl_servidor}/chain.pem`) });

// MIDDLEWARES DO SERVIDOR
server.pre(cors.preflight);
server.use(cors.actual);
server.use(helmet());
server.use(restify.plugins.bodyParser({ limit: '100mb' }));
server.use(trim);
server.get('/public/*', restify.plugins.serveStatic({ directory: __dirname }));

// CRIAÇÃO DE LOGS DE ACESSO
server.use(morgan('(:date[web]) - :remote-addr - :method :status :url - :response-time ms', { stream: fs.createWriteStream(path.join(__dirname, `access.log`), { flags: 'a' }), skip: function (req, res) { return res.statusCode < 400 } }));
if (process.env.NODE_ENV === 'development') server.use(morgan('dev'));

// ROUTES
commonRouter.applyRoutes(server);
userRouter.applyRoutes(server, '/users');
// END ROUTES

// 404 ERROR HANDLER
server.on('NotFound', (req, res, err, next) => { res.status(404); res.json({ error: { message: 'Não encontrado.' }, status: 404 }); });

// INICIAR SERVIDOR HTTPS
const port = process.env.PORT || 3000;
server.listen(port, () => console.log(`Server is listening HTTPS on port: ${port}`));