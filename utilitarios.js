'use strict'; 

/**
 * Função para normalizar uma data/hora específica sem alterar hora e mantendo em Objeto de data
 */
const normalizeUTC = (data) => {
	const tempDate = Date.UTC(data.getFullYear(), data.getMonth(), data.getDate(), data.getHours(), data.getMinutes(), data.getSeconds(), data.getMilliseconds());
	return new Date(tempDate);
}

module.exports = {

	/**
	 * Função error handler
	 */
	showError: (res, error, status = 500) => {
		res.status(status);
		return res.json({ error: { message: error }, status });
	},

	/**
	 * Função forEach podendo usar Async/Await
	 * Modo de uso: asyncForEach(array, (arr) => { console.log(arr); });
	 */
	asyncForEach: async (array, callback) => {
        for (let index = 0; index < array.length; index++) {
            await callback(array[index], index, array);
        }
	},

	/**
	 * Para normalizar data sem formatar nem manipular horas
	 * Modo de uso: const hoje = normalizeDateUTC(new Date());
	 */
	normalizeDateUTC: (data) => { return normalizeUTC(data); },
	
	/**
	 * Formatar Objeto de Data em Estilo Americano: yyyy-mm-dd
	 * Modo de uso: const hoje = toEua(new Date());
	 */
	toEua: (data) => {
		data = normalizeUTC(data);
		const dia = data.getUTCDate() < 10 ? '0'+data.getUTCDate() : data.getUTCDate();
		const mes = (data.getUTCMonth()+1) < 10 ? '0'+(data.getUTCMonth()+1) : (data.getUTCMonth()+1);
		const ano = data.getUTCFullYear();
		return ano+'-'+mes+'-'+dia;
	},

	/**
	 * Formatar Objeto de Data em Estilo Brasileiro: dd/mm/yyyy
	 * Modo de uso: const hoje = toBr(new Date());
	 */
	toBr: (data) => {
		data = normalizeUTC(data);
		const dia = data.getUTCDate() < 10 ? '0'+data.getUTCDate() : data.getUTCDate();
		const mes = (data.getUTCMonth()+1) < 10 ? '0'+(data.getUTCMonth()+1) : (data.getUTCMonth()+1);
		const ano = data.getUTCFullYear();
		return dia+'/'+mes+'/'+ano;
	},

	/**
	 * Fazer qualquer Objeto de data ficar com a Hora/Minuto/Segundo/Milesegundos igual a 0
	 * Modo de uso: const hojeZerado = toZeroTime(new Date());
	 */
	toZeroTime: (data) => {
		data = normalizeUTC(data);
		data.setHours(0);
		data.setMinutes(0);
		data.setSeconds(0);
		data.setMilliseconds(0);
		return data;
	},

	/**
	 * Cria o objeto de conexão do SMTP
	 */
	getTransport: async (nodemailer) => {
		let transp = await nodemailer.createTransport({
			host: process.env.SMTP_HOST,
			port: 587,
			secure: false,
			auth: {
				user: process.env.SMTP_USER,
				pass: process.env.SMTP_PASS
			}
		});
		return transp;
	}
}