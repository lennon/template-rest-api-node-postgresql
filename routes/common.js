'use strict'; 
const Router = require('restify-router').Router;  
const router = new Router();

const CommonController = require('../controllers/common');

router.get('/', CommonController.something);

module.exports = router;