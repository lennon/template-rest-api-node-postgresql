'use strict'; 
const { showError } = require('../utilitarios');
const jwt = require('jsonwebtoken');

const Router = require('restify-router').Router;  
const router = new Router();

const UsersController = require('../controllers/users');

const authMiddleware = async (req, res, next) => {
    res.locals = {};
    try {
        const { authorization } = req.headers;
        if ( !authorization ) throw ('Sem token');
        const parts = authorization.split(' ');
        if ( !parts.length === 2 ) throw ('Erro de token');
        const [ scheme, token ] = parts;
        if ( !/^Bearer$/i.test(scheme) ) throw ('Token mal formado');
        await jwt.verify(token, process.env.SECRET_KEY, (err, decoded) => {
            if(err) throw ('Token inválido');
            if(decoded && decoded.user && decoded.user.id) res.locals.userId = decoded.user.id;
            next();
        });
    } catch (error) {
        showError(res, error);
    }
}

router.post('/login', UsersController.login);

router.get('/', authMiddleware, UsersController.getAll);
router.post('/', authMiddleware, UsersController.newUser);

router.get('/:id', authMiddleware, UsersController.getUser);
router.put('/:id', authMiddleware, UsersController.updateUser);

module.exports = router;